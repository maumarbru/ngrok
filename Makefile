image := ngrok_mauro
container := ngrok_mauro_service
authtoken :=

.PHONY: build  # Build the docker image
build:
	@docker build -t $(image) --build-arg authtoken=$(authtoken) .

.PHONY: run  # Run the service interactivelly
run:
	@docker run --rm --net=host -t $(image) ngrok start --all

.PHONY: shell
shell:
	@docker run --rm --net=host -ti $(image) bash

.PHONY: install  # Run ngrok all services and reastart unless stopped
install:
	@docker run --name $(container) -t --net=host -d --restart unless-stopped $(image)

.PHONY: logs
logs:
	@docker container logs -f $(container)

.PHONY: start
start:
	@docker container start $(container)

.PHONY: stop
stop:
	@docker container stop $(container) || true

.PHONY: remove
remove: stop
	@docker container rm $(container) || true

.PHONY: clean
clean: remove
	@docker image rm $(image)
