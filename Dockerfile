from debian:latest
arg authtoken
env USER=ngrok
env HOME /home/$USER
workdir $HOME
copy ngrok /usr/bin/
copy ngrok.yml /home/ngrok/.config/ngrok/ngrok.yml
run chown 1000:1000 -R /home/ngrok
user 1000
cmd ngrok start --all --log stdout
run ngrok config add-authtoken $authtoken
