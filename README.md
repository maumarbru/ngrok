# ngrok in docker

## Configuration

The ngrok services are configured in [ngrok.yml](ngrok.yml). For default an ssh
tunnel it's defined.

## ngrok binary

The [ngrok](ngrok) contained in this repo has been download from https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-linux-amd64.tgz. Anyway if you don't trust on it you can download it yourserf from ngrok.com and repace it.

## Requirements
- Docker
- Make
> If your user isn't part of the docker group, you should call with `sudo`
> every `make` command.

## Build the docker image
	make build authtoken=<your-ngrok-authtoken>

### Try it
	make run

### Install the container service
	make install

#### Service mantainance
	make logs    # to see the container logs.
	make stop    # to stop the container.
	make start   # to start the container (an stopped container).
	make shell   # to run an interactive bash sesion inside a new container.

### Remove the container
	make remove  # to remove the container

## Clean all
	make clean   # to remove the container and the docker image
